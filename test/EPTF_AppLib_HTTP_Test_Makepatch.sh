#!/bin/sh

sed -e '

s/CPPFLAGS = -D$(PLATFORM) -I$(TTCN3_DIR)\/include/CPPFLAGS = -D$(PLATFORM) -I$(OPENSSL_DIR)\/include -I$(TTCN3_DIR)\/include -DNO_IPV6/g

s/OPENSSL_DIR = $(TTCN3_DIR)/#OPENSSL_DIR = $(TTCN3_DIR)/g

#s/CXX = g++/CXX = \/usr\/sfw\/bin\/g++/g
#s/CPP = cpp/CPP = \/usr\/sfw\/bin\/cpp/g

#s/-L$(OPENSSL_DIR)\/lib -lcrypto $($(PLATFORM)_LIBS)/-L$(OPENSSL_DIR)\/lib -lcrypto $($(PLATFORM)_LIBS)/g

s/SOLARIS_LIBS = -lsocket -lnsl/SOLARIS_LIBS = -lsocket -lnsl -lresolv -lxnet/g

s/SOLARIS8_LIBS = -lsocket -lnsl/SOLARIS8_LIBS = -lsocket -lnsl -lresolv -lxnet/g

s/CXXFLAGS = -Wall/CXXFLAGS = -Wall /g

#s/LDFLAGS =/LDFLAGS = -g/g

' <$1 >$2
